import generador
import profundidad
import profundidad_limitada
import a_star_path_finding as p
import time
import sys

N=int(sys.argv[1])
m=float(sys.argv[2])
l=int(sys.argv[3])

a = p.AStar()

tablero, obstaculos = generador.generarLaberinto(N, m)
print("Tablero generado:")
print(tablero)
print()

print("Algoritmo de profundidad")
tab_profundidad = tablero.copy()
start = time.time()
profundidad.search(tab_profundidad, 0, 0)
end = time.time()
tiempo_profundidad = end - start
print(tab_profundidad)
print("Tiempo: ",tiempo_profundidad)
print()

print("Algoritmo de profundidad limitada")
tab_limitada = tablero.copy()
start = time.time()
print(profundidad_limitada.recorrer(tab_limitada, 0, 0, l))
end = time.time()
tiempo_limitada = end - start
print(tab_limitada)
print("Tiempo: ",tiempo_limitada)
print()

print("Algoritmo A*")
start = time.time()
a.init_grid(N, N, obstaculos, (0, 0), (N-1, N-1))
camino = a.solve()
end = time.time()
tiempo_A = end - start
print("Camino recorrido")
print(camino)
print("Tiempo: ",tiempo_A)