import a_star_path_finding as pf
import generador

a = pf.AStar()

N  = 5
grid, walls = generador.generarLaberinto(N, 0.4)

# walls = ((0, 5), (1, 0), (1, 1), (1, 5), (2, 3),
#             (3, 1), (3, 2), (3, 5), (4, 1), (4, 4), (5, 1))
a.init_grid(N, N, walls, (0, 0), (N-1, N-1))
path = a.solve()
print(grid)
print(path)