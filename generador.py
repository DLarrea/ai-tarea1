import numpy
import random

def search(x, y, grid):
    if grid[x][y] == 2:
        return True
    elif grid[x][y] == 1:
        return False
    elif grid[x][y] == 3:
        return False
     
    grid[x][y] = 3
 
    if ((x < len(grid)-1 and search(x+1, y, grid))
        or (y > 0 and search(x, y-1, grid))
        or (x > 0 and search(x-1, y, grid))
        or (y < len(grid)-1 and search(x, y+1, grid))):
        return True
    return False


def generarLaberinto(dim, wallsFactor):
    valid = False
    N = dim
    W = wallsFactor
    walls = int(N * N * W)

    wallsMap = []

    while not valid:
        wallsMap = []
        grid = numpy.random.randint(1, size=(N, N))
        
        for i in range(walls):
            
            x = random.randint(0, N-1)
            y = random.randint(0, N-1)

            while(x+y == 0 or x+y == 2*(N-1) or grid[x][y] == 1):
                
                x = random.randint(0, N-1)
                y = random.randint(0, N-1)

            wallsMap.append((x,y))
            grid[x][y] = 1

        grid[N-1, N-1] = 2

        valid = search(0,0, grid)
        
    for i in range(N):
        for j in range(N):
            if grid[i][j] == 3:
                grid[i][j] = 0

    return grid, wallsMap