
def recorrer(tablero, x, y, l):
    if tablero[x][y] == 2:
        return True, 1
    elif tablero[x][y] == 1:
        return False, 1
    elif tablero[x][y] == 3:
        return False, 1
     
    #marcar como visitado
    tablero[x][y] = 3
 
    if l == 1:
        return False, 1

    logout = False
    niveles = 1
    if (x < len(tablero)-1 and not logout):
        log, val = recorrer(tablero, x+1, y, l-1)
        logout = logout or log
        niveles = niveles + val
    if (y > 0 and not logout):
        log, val = recorrer(tablero, x, y-1, l-1)
        logout = logout or log
        niveles = niveles + val
    if (x > 0 and not logout):
        log, val = recorrer(tablero, x-1, y, l-1)
        logout = logout or log
        niveles = niveles + val
    if (y < len(tablero)-1 and not logout):
        log, val = recorrer(tablero, x, y+1, l-1)
        logout = logout or log
        niveles = niveles + val
    return logout, niveles

