
def search(tablero, x, y):
    if tablero[x][y] == 2:
        return True
    elif tablero[x][y] == 1:
        return False
    elif tablero[x][y] == 3:
        return False
     
    # mark as visited
    tablero[x][y] = 3
 
    # explore neighbors clockwise starting by the one on the right
    if ((x < len(tablero)-1 and search(tablero, x+1, y))
        or (y > 0 and search(tablero, x, y-1))
        or (x > 0 and search(tablero, x-1, y))
        or (y < len(tablero)-1 and search(tablero, x, y+1))):
        return True
    return False
